﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObstacleRemover
{
	public struct DifficultyLevel : IEquatable<DifficultyLevel>
	{
		public string audioPath { get; set; }
		public string difficulty { get; set; }
		public int difficultyRank { get; set; }
		public string jsonPath { get; set; }
		public int offset { get; set; }
		public int oldOffset { get; set; }

		public override bool Equals(object obj)
		{
			return obj is DifficultyLevel && Equals((DifficultyLevel)obj);
		}

		public bool Equals(DifficultyLevel other)
		{
			return audioPath == other.audioPath &&
						 difficulty == other.difficulty &&
						 difficultyRank == other.difficultyRank &&
						 jsonPath == other.jsonPath &&
						 offset == other.offset &&
						 oldOffset == other.oldOffset;
		}

		public override int GetHashCode()
		{
			var hashCode = -1111756561;
			hashCode = hashCode * -1521134295 + base.GetHashCode();
			hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(audioPath);
			hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(difficulty);
			hashCode = hashCode * -1521134295 + difficultyRank.GetHashCode();
			hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(jsonPath);
			hashCode = hashCode * -1521134295 + offset.GetHashCode();
			hashCode = hashCode * -1521134295 + oldOffset.GetHashCode();
			return hashCode;
		}

		public static bool operator ==(DifficultyLevel level1, DifficultyLevel level2)
		{
			return level1.Equals(level2);
		}

		public static bool operator !=(DifficultyLevel level1, DifficultyLevel level2)
		{
			return !(level1 == level2);
		}
	}

	public struct BeatInfo : IEquatable<BeatInfo>
	{
		public string authorName { get; set; }
		public double beatsPerMinute { get; set; }
		public string coverImagePath { get; set; }
		public List<DifficultyLevel> difficultyLevels { get; set; }
		public string environmentName { get; set; }
		public double previewDuration { get; set; }
		public double previewStartTime { get; set; }
		public string songName { get; set; }
		public string songSubName { get; set; }

		public override bool Equals(object obj)
		{
			return obj is BeatInfo && Equals((BeatInfo)obj);
		}

		public bool Equals(BeatInfo other)
		{
			return authorName == other.authorName &&
						 beatsPerMinute == other.beatsPerMinute &&
						 coverImagePath == other.coverImagePath &&
						 EqualityComparer<List<DifficultyLevel>>.Default.Equals(difficultyLevels, other.difficultyLevels) &&
						 environmentName == other.environmentName &&
						 previewDuration == other.previewDuration &&
						 previewStartTime == other.previewStartTime &&
						 songName == other.songName &&
						 songSubName == other.songSubName;
		}

		public override int GetHashCode()
		{
			var hashCode = 1182164595;
			hashCode = hashCode * -1521134295 + base.GetHashCode();
			hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(authorName);
			hashCode = hashCode * -1521134295 + beatsPerMinute.GetHashCode();
			hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(coverImagePath);
			hashCode = hashCode * -1521134295 + EqualityComparer<List<DifficultyLevel>>.Default.GetHashCode(difficultyLevels);
			hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(environmentName);
			hashCode = hashCode * -1521134295 + previewDuration.GetHashCode();
			hashCode = hashCode * -1521134295 + previewStartTime.GetHashCode();
			hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(songName);
			hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(songSubName);
			return hashCode;
		}

		public static bool operator ==(BeatInfo info1, BeatInfo info2)
		{
			return info1.Equals(info2);
		}

		public static bool operator !=(BeatInfo info1, BeatInfo info2)
		{
			return !(info1 == info2);
		}
	}

	public struct BeatEvent : IEquatable<BeatEvent>
	{
		public double _time { get; set; }
		public int _type { get; set; }
		public int _value { get; set; }

		public override bool Equals(object obj)
		{
			return obj is BeatEvent && Equals((BeatEvent)obj);
		}

		public bool Equals(BeatEvent other)
		{
			return _time == other._time &&
						 _type == other._type &&
						 _value == other._value;
		}

		public override int GetHashCode()
		{
			var hashCode = 1326033444;
			hashCode = hashCode * -1521134295 + base.GetHashCode();
			hashCode = hashCode * -1521134295 + _time.GetHashCode();
			hashCode = hashCode * -1521134295 + _type.GetHashCode();
			hashCode = hashCode * -1521134295 + _value.GetHashCode();
			return hashCode;
		}

		public static bool operator ==(BeatEvent e1, BeatEvent e2)
		{
			return e1.Equals(e2);
		}

		public static bool operator !=(BeatEvent e1, BeatEvent e2)
		{
			return !e1.Equals(e2);
		}
	}

	public struct BeatNote : IEquatable<BeatNote>
	{
		public int _cutDirection { get; set; }
		public int _lineIndex { get; set; }
		public int _lineLayer { get; set; }
		public double _time { get; set; }
		public int _type { get; set; }

		public override bool Equals(object obj)
		{
			return obj is BeatNote && Equals((BeatNote)obj);
		}

		public bool Equals(BeatNote other)
		{
			return _cutDirection == other._cutDirection &&
						 _lineIndex == other._lineIndex &&
						 _lineLayer == other._lineLayer &&
						 _time == other._time &&
						 _type == other._type;
		}

		public override int GetHashCode()
		{
			var hashCode = -387699751;
			hashCode = hashCode * -1521134295 + base.GetHashCode();
			hashCode = hashCode * -1521134295 + _cutDirection.GetHashCode();
			hashCode = hashCode * -1521134295 + _lineIndex.GetHashCode();
			hashCode = hashCode * -1521134295 + _lineLayer.GetHashCode();
			hashCode = hashCode * -1521134295 + _time.GetHashCode();
			hashCode = hashCode * -1521134295 + _type.GetHashCode();
			return hashCode;
		}

		public static bool operator ==(BeatNote n1, BeatNote n2)
		{
			return n1.Equals(n2);
		}

		public static bool operator !=(BeatNote n1, BeatNote n2)
		{
			return !n1.Equals(n2);
		}
	}

	public struct BeatObstacle : IEquatable<BeatObstacle>
	{
		public double _duration { get; set; }
		public int _lineIndex { get; set; }
		public double _time { get; set; }
		public int _type { get; set; }
		public int _width { get; set; }

		public override bool Equals(object obj)
		{
			return obj is BeatObstacle && Equals((BeatObstacle)obj);
		}

		public bool Equals(BeatObstacle other)
		{
			return _duration == other._duration &&
						 _lineIndex == other._lineIndex &&
						 _time == other._time &&
						 _type == other._type &&
						 _width == other._width;
		}

		public override int GetHashCode()
		{
			var hashCode = 200625281;
			hashCode = hashCode * -1521134295 + base.GetHashCode();
			hashCode = hashCode * -1521134295 + _duration.GetHashCode();
			hashCode = hashCode * -1521134295 + _lineIndex.GetHashCode();
			hashCode = hashCode * -1521134295 + _time.GetHashCode();
			hashCode = hashCode * -1521134295 + _type.GetHashCode();
			hashCode = hashCode * -1521134295 + _width.GetHashCode();
			return hashCode;
		}

		public static bool operator ==(BeatObstacle ob1, BeatObstacle ob2)
		{
			return ob1.Equals(ob2);
		}

		public static bool operator !=(BeatObstacle ob1, BeatObstacle ob2)
		{
			return !ob1.Equals(ob2);
		}
	}

	public struct BeatSaberSong : IEquatable<BeatSaberSong>
	{
		public int _beatsPerBar { get; set; }
		public double _beatsPerMinute { get; set; }
		public List<BeatEvent> _events { get; set; }
		public int _noteJumpSpeed { get; set; }
		public List<BeatNote> _notes { get; set; }
		public List<BeatObstacle> _obstacles { get; set; }
		public int _shuffle { get; set; }
		public double _shufflePeriod { get; set; }
		public string _version { get; set; }

		public override bool Equals(object obj)
		{
			return obj is BeatSaberSong && Equals((BeatSaberSong)obj);
		}

		public bool Equals(BeatSaberSong other)
		{
			if (other == null) return false;
			if (_beatsPerBar != other._beatsPerBar) return false;
			if (_beatsPerMinute != other._beatsPerMinute) return false;
			if (EqualityComparer<List<BeatEvent>>.Default.Equals(_events, other._events)) return false;
			if (_noteJumpSpeed != other._noteJumpSpeed) return false;
			if (EqualityComparer<List<BeatNote>>.Default.Equals(_notes, other._notes)) return false;
			if (EqualityComparer<List<BeatObstacle>>.Default.Equals(_obstacles, other._obstacles)) return false;
			if (_shuffle != other._shuffle) return false;
			if (_shufflePeriod != other._shufflePeriod) return false;
			if (_version != other._version) return false;
			return true;
		}

		public override int GetHashCode()
		{
			var hashCode = 191277127;
			hashCode = hashCode * -1521134295 + _beatsPerBar.GetHashCode();
			hashCode = hashCode * -1521134295 + _beatsPerMinute.GetHashCode();
			hashCode = hashCode * -1521134295 + EqualityComparer<List<BeatEvent>>.Default.GetHashCode(_events);
			hashCode = hashCode * -1521134295 + _noteJumpSpeed.GetHashCode();
			hashCode = hashCode * -1521134295 + EqualityComparer<List<BeatNote>>.Default.GetHashCode(_notes);
			hashCode = hashCode * -1521134295 + EqualityComparer<List<BeatObstacle>>.Default.GetHashCode(_obstacles);
			hashCode = hashCode * -1521134295 + _shuffle.GetHashCode();
			hashCode = hashCode * -1521134295 + _shufflePeriod.GetHashCode();
			hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_version);
			return hashCode;
		}

		public static bool operator ==(BeatSaberSong song1, BeatSaberSong song2)
		{
			return EqualityComparer<BeatSaberSong>.Default.Equals(song1, song2);
		}

		public static bool operator !=(BeatSaberSong song1, BeatSaberSong song2)
		{
			return !(song1 == song2);
		}
	}
}
