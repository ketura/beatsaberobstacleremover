﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

using Newtonsoft.Json;

namespace ObstacleRemover
{
	public enum DeletionType
	{
		None,
		Revert,
		DeleteHorizontals,
		DeleteVerticals,
		DeleteAll
	}

  public class SongProcessor
  {
		public static string BackupExtension = ".WithObstacles";
		public static string HorizWallSuffix = " [No Horiz Walls]";
		public static string VertWallSuffix = " [No Vert Walls]";
		public static string AllWallSuffix = " [No Walls]";

		public string SongLocation { get; protected set; }
		public DeletionType Deletion { get; protected set; }

		private void BackupFile(string filename)
		{
			string backupFile = $"{filename}{BackupExtension}";
			if (File.Exists(backupFile))
			{
				File.Delete(backupFile);
			}

			File.Move(filename, $"{filename}{BackupExtension}");
		}

		public void Process()
		{
			if (Deletion == DeletionType.None)
				return;

			if(Deletion == DeletionType.Revert)
			{
				Revert();
				return;
			}

			Dictionary<string, BeatSaberSong> songs = new Dictionary<string, BeatSaberSong>();
			foreach(string filename in Directory.GetFiles(SongLocation, "*.json", SearchOption.AllDirectories))
			{
				if (Path.GetFileName(filename) == "info.json" || filename.Contains("autosaves"))
					continue;

				string file = File.ReadAllText(filename);
				BeatSaberSong song = JsonConvert.DeserializeObject<BeatSaberSong>(file);
				if(song._obstacles == null)
				{
					continue;
				}

				if (song._obstacles.Count == 0)
				{
					Console.WriteLine($"Song filename {filename} did not have any obstacles, leaving it alone.");
					continue;
				}

				BackupFile(filename);

				string titleSuffix = "";

				switch (Deletion)
				{
					case DeletionType.DeleteHorizontals:
						song._obstacles = song._obstacles.Where(x => x._type == 0).ToList();
						if (song._obstacles.Count > 0)
						{
							titleSuffix = HorizWallSuffix;
						}
						else
						{
							titleSuffix = AllWallSuffix;
						}
						break;
					case DeletionType.DeleteVerticals:
						song._obstacles = song._obstacles.Where(x => x._type == 1).ToList();
						if(song._obstacles.Count > 0)
						{
							titleSuffix = VertWallSuffix;
						}
						else
						{
							titleSuffix = AllWallSuffix;
						}
						
						break;
					case DeletionType.DeleteAll:
						song._obstacles.Clear();
						titleSuffix = AllWallSuffix;
						break;
				}

				string infoName = Path.Combine(Path.GetDirectoryName(filename), "info.json");
				if (File.Exists(infoName) && !File.Exists($"{infoName}{BackupExtension}"))
				{
					var info = JsonConvert.DeserializeObject<BeatInfo>(File.ReadAllText(infoName));
					BackupFile(infoName);

					info.songName += titleSuffix;
					File.WriteAllText(infoName, JsonConvert.SerializeObject(info));
				}

				string newjson = JsonConvert.SerializeObject(song);
				File.WriteAllText(filename, newjson);

				songs.Add(filename, song);
			}
		}

		public void Revert()
		{
			foreach (string filename in Directory.GetFiles(SongLocation, $"*{BackupExtension}", SearchOption.AllDirectories))
			{
				string original = filename.Replace($"{BackupExtension}", "");
				File.Delete(original);
				File.Move(filename, original);
			}
		}

		public SongProcessor(string songLocation, DeletionType deletionType)
		{
			SongLocation = songLocation;
			Deletion = deletionType;
		}
  }
}
