﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;

using Newtonsoft.Json;

namespace ObstacleRemover
{
	public class Program
	{
		static void Main(string[] args)
		{
			string settingsFilename = "settings.json";
			if (!File.Exists(settingsFilename))
			{
				Console.WriteLine("No settings file detected; regenerating settings.");
				File.WriteAllText(settingsFilename, JsonConvert.SerializeObject(new Settings()));
			}
			
			Settings settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText(settingsFilename));

			while (String.IsNullOrWhiteSpace(settings.SongPath))
			{
				Console.WriteLine("\n\nPlease enter the folder location holding songs you wish to remove walls from: ");
				string path = Console.ReadLine();
				if (!Directory.Exists(path))
				{
					Console.WriteLine("Invalid directory path, or it's a path this program does not have access to (try running as Administrator if the path is indeed correct).");
				}
				else
				{
					settings.SongPath = path;
				}
			}

			File.WriteAllText(settingsFilename, JsonConvert.SerializeObject(settings));

			Console.Clear();

			Console.WriteLine("********WARNING********");
			Console.WriteLine($"MAKE SURE TO BACK UP ALL YOUR SONGS IN THE FOLDER '{settings.SongPath}' BEFORE CONTINUING.");
			Console.WriteLine("\nYou have been warned.");


			int input = 0;
			while(input == 0)
			{
				Console.WriteLine("\nPlease choose one of the following options:");

				Console.WriteLine("1 - Delete all horizontal walls from all song files");
				Console.WriteLine("2 - Delete all vertical walls from all song files");
				Console.WriteLine("3 - Delete ALL walls from all song files");
				Console.WriteLine("4 - Revert the most recent wall deletion (if the backup files exist)");
				Console.WriteLine("5 - Exit");

				if(!Int32.TryParse(Console.ReadLine(), out input) || input < 1 || input > 5)
				{
					Console.Clear();
					Console.WriteLine("Invalid input!  Please enter a single digit from 1 to 5 and press Enter.");
				}
			}

			DeletionType deletion = DeletionType.None;

			switch (input)
			{
				case 1:
					deletion = DeletionType.DeleteHorizontals;
					break;
				case 2:
					deletion = DeletionType.DeleteVerticals;
					break;
				case 3:
					deletion = DeletionType.DeleteAll;
					break;
				case 4:
					deletion = DeletionType.Revert;
					break;
				case 5:
					deletion = DeletionType.None;
					break;
			}


			//var processor = new SongProcessor(@"X:\Games\SteamLibrary\steamapps\common\Beat Saber\CustomSongs");
			var processor = new SongProcessor(settings.SongPath, deletion);
			processor.Process();
		}
	}
}
