## BeatSaberObstacleRemover

Used for removing obstacles from Beat Saber maps.

To use, run ObstacleRemover.exe and follow the prompts.  Be sure to back up your song files before use (if you lose data, don't come crying to me).  

The menu will permit you to remove horizontal walls, vertical walls, or both.  

When fixing song files, a backup will be made using the filename of the original and ".WithObstacles" tacked onto the end.  

If you wish to restore the original files, use the Revert menu option, or simply delete the appropriate \*.json file and remove the .WithObstacles ending.

The first time you run the program, you will be prompted to provide the folder to search for song files in.  This search is recursive, so if you provide the CustomSongs directory, this will affect *all* songs.  

On successive runs, either delete the Settings.json file to be re-prompted or edit the file directly.

Mutated songs also have their titles changed, so it's more obvious in-game (and on leaderboards) that the walls have been removed.  

This has been tested on Windows 10 only, using .NET Framework 4.5.  Other Windows OSs *should* work, and this will likely work on Mac/Linux with mono.